{
  description = "A simple Nix Tutor to help the Uninitiated";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ ];
          config = {
            allowUnfree = true;
          };
        };
        
        lessonsSrc = ./lessons;
          
          lessonsPackages = let
            files = builtins.readDir lessonsSrc;
            makeLessonPkg = name: path: pkgs.stdenv.mkDerivation {
              name = "lesson-${name}";
              src = path;
              installPhase = ''
                mkdir -p $out/src
                cp $src $out/src/${name}
              '';
            };
          in builtins.mapAttrs makeLessonPkg files;
          
          tutorScripts = builtins.mapAttrs (name: pkg: pkgs.writeScriptBin (name) ''
            clear
            ${pkgs.bat}/bin/bat -l md --theme OneHalfDark -p ${pkg}/src/${name}
          '') lessonsPackages;

          menuScript = pkgs.writeShellScriptBin "tutor-menu" ''
            # Define ANSI color codes
            RED='\033[0;31m'
            GREEN='\033[0;32m'
            BLUE='\033[0;34m'
            NC='\033[0m' # No Color

            # Clear the screen
            clear

            # Menu header
            echo -e "''${GREEN}================================''${NC}"
            echo -e "''${GREEN}Welcome to the Nix Tutor Menu''${NC}"
            echo -e "''${GREEN}================================''${NC}"
            echo "Please select a lesson to view, or choose the last option to exit:"

            lessons=(${builtins.concatStringsSep " " (builtins.attrNames lessonsPackages)})

            # Dynamically generate options, removing the numeric prefix and '-lesson-' part
            options=()
            for lesson in "''${lessons[@]}"; do
              # Use parameter expansion to modify the lesson name
              displayName=''${lesson#*-lesson-}
              options+=("$displayName")
            done
            options+=("Exit") # Add 'Exit' as the last option

            select opt in "''${options[@]}"
            do
              if [[ "$REPLY" == "''${#options[@]}" ]]; then
                echo -e "''${RED}Exiting...''${NC}"
                exit 0
              elif [[ "$REPLY" -ge 1 && "$REPLY" -lt "''${#options[@]}" ]]; then
                # Adjust index for array access to get the original lesson name
                index=$(($REPLY-1))
              lessonName=''${lessons[$index]}
              echo -e "''${BLUE}Displaying lesson: ''${opt}''${NC}"
              clear
              ${pkgs.bat}/bin/bat -l md --theme OneHalfDark -p ${lessonsSrc}/$lessonName 
            else
              echo -e "''${RED}Invalid option, please try again.''${NC}"
            fi
            break
          done
        '';

        
      in
      {
        packages = tutorScripts // { menu = menuScript; };
        defaultPackage = menuScript;
      }
    );
}

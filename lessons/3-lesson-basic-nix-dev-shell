# Nix Develop Shells: A Beginner's Guide

Nix develop shells provide an isolated development environment with specific packages and tools, without affecting the global system. This guide demonstrates creating a basic Nix shell environment.

1. **Create `shell.nix`**: This file specifies the environment. Start by creating a `shell.nix` file in your project directory:

```nix
{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.python3
    pkgs.git
  ];
  shellHook = ''
    echo "Welcome to your Nix dev shell!"
  '';
}
```

This shell includes Python3 and Git. Modify `buildInputs` to include the packages your project needs.

2. **Enter the Nix Shell**: Navigate to your project directory in the terminal and run `nix-shell`. This command reads your `shell.nix` and creates the environment.

3. **Using the Shell**: Inside the shell, the specified packages (e.g., Python3, Git) are available. You can start developing with these tools immediately.

*Note: try running `which python` or `which git` and you will see the path to the executable resides in the `/nix/store`.*

4. **Exiting the Shell**: To leave the shell, simply type `exit`.

This simple setup demonstrates creating a Nix develop shell. Customize `shell.nix` as needed for different projects. Nix shells are powerful for creating reproducible development environments.

# Nix Tutor

Are you new to Nix? Have your friends been nagging you about giving Nix a try? 

Do you not know where to begin? 

This is an attempt at helping the uninitiated get started with Nix in a practical way.

Some inspiration was taken from Vim Tutor and this is still very much a work in progress, 
so feed back is greatly appreciated in the form of issues.


## Requirements 

The only requirement is that you have Nix installed. This can be done with the following command.


```bash
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
```

## Now what?!

Just run this command to begin the Tutor. It will provide you a menu of lessons. They are meant to
be done sequentially but that is up to you. 

```
nix run gitlab:usmcamp0811/nix-tutor#menu
```

If at anytime you want to return to the menu you can just re-run the above command.
